#skin.xperience1080

Removes the mouse warning on the Xperience1080 theme, and replaces it with a mouse cursor.

How to use:
- Drop skin.xperience1080 folder into your Kodi addons folder
- Allow the overwriting of the original files
- Reload skin, restart Kodi, or switch to the xperience1080 skin
- Enjoy a mouse, rather than the annoying popup :smile:

##Default Mouse Cursor
![Preview of Xperience1080 default cursor](http://oi60.tinypic.com/153n235.jpg)

##Changed Mouse Cursor
![Preview of Xperience1080 modified cursor](http://i.imgur.com/sYdhKMF.png)
